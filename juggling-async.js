const http = require('http')
const bl = require('bl')
const results = []
let counter = 0

function printResults () {
  for (let i = 0; i < 3; i++) {
    console.log(results[i])
  }
}

function httpGet (index) {
  http.get(process.argv[2 + index], function (res) {
    res.pipe(bl(function (err, value) {
      if (err) {
        return console.error(err)
      }

      results[index] = value.toString()
      counter++

      if (counter === 3) {
        printResults()
      }
    }))
  })
}

for (let i = 0; i < 3; i++) {
  httpGet(i)
}