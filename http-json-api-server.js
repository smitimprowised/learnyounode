const http = require("http")


function parsetime (time) {
  return {

    hour: time.getHours(),
    minute: time.getMinutes(),
    second: time.getSeconds()
  }
}

function unixtime (time) {
  return { unixtime: time.getTime() }
}


var server = http.createServer((req,res) => {

   var url = new URL(req.url,'http://example.com')
   var time = new Date(url.searchParams.get('iso'))
   let value

  if (/^\/api\/parsetime/.test(req.url)) {
    value = parsetime(time)

  } else if (/^\/api\/unixtime/.test(req.url)) {

  	value = unixtime(time)
  }


   if (value)
  {
   res.writeHead(200,{'content-type':'application/json'});
   res.end(JSON.stringify(value));
   }
})

server.listen(Number(process.argv[2]))