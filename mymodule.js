const fs = require ("fs")
const path=require("path")

module.exports = function(dir,mymodule,callback) {

fs.readdir(dir,(err,lists)=>{

	if(err) return callback(err)
	lists=lists.filter((file)=>{ return path.extname(file)=== '.' + mymodule})
    callback(null,lists)
 
})

}